package users;

public class User {
	private String name;
	private int points;
	private String currentMission; // Si es un nuevo usuario, carga la primera misi�n
	private boolean oldUser; // Checks if the points and the mission is greater than 1.
	
	// Constructor for the user
	
	public User(String name, int points, String mission, boolean oldUser) {
		this.name = name;
		this.points = points;
		this.currentMission = mission;
		this.oldUser = oldUser;
	}
	
	// Get User: Devuelve un objeto usuario
	
	public User getUser() {
		return this;
	}
	
	// Getters and setters

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPoints() {
		return points;
	}
	
	public void setPoints(int points) {
		this.points = points;
	}

	public String getCurrentMission() {
		return currentMission;
	}

	public void setCurrentMission(String currentMission) {
		this.currentMission = currentMission;
	}

	public boolean isOldUser() {
		return oldUser;
	}

	public void setOldUser(boolean oldUser) {
		this.oldUser = oldUser;
	}
	
}
